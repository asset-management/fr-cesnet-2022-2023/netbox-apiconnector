from setuptools import find_packages, setup

setup(
    name='netbox-apiconnector',
    version='0.1.7',
    description='Netbox Plugin - ApiConnector',
    long_description='Netbox Plugin - ApiConnector',
    url='https://gitlab.ics.muni.cz/asset-management/fr-cesnet-2022-2023/netbox-apiconnector',
    download_url='https://www.pypi.org/project/netbox-apiconnector/',
    author='Roman Mariancik',
    author_email='492965@mail.muni.cz',
    license='Apache 2.0',
    install_requires=[
    ],
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Framework :: Django",
    ],
    python_requires=">=3.9",
    zip_safe=False,
)
