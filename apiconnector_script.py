from extras.scripts import *
from netbox_apiconnector.models import ApiConnector
from netbox_apiconnector.api_connector import dynamic_converter, static_converter, apply_results_by_pk
from netbox_apiconnector.status_codes import *

__version__ = "0.1.0"
__author__ = "Roman Mariancik"

# Timeout for the report run
JOB_TIMEOUT = 3600


class RunApiConnector(Script):
    job_timeout = JOB_TIMEOUT

    class Meta:
        name = "Run Api Connector"
        description = "Run API connector periodically"
        field_order = ["connector"]

    use_pattern_mapping = BooleanVar(description="If checked, uses pattern URL")
    apply_changes = BooleanVar(description="If checked, applies all changes to the database. USE WITH CAUTION")
    connector = ObjectVar(
        model=ApiConnector,
        required=True,
        label="Api connector",
    )

    def run(self, data, commit):
        connector = data['connector']
        try:
            if data['use_pattern_mapping']:
                result = dynamic_converter(connector)
            else:
                result = static_converter(connector)
        except Exception as e:
            set_status(connector, FAIL, e)
            return f"FAIL: {e}"
        if data['apply_changes']:
            fails_lst = apply_results_by_pk([result.pk])
            if len(fails_lst) > 0 and fails_lst[0] == 0:
                return f"OK, {len(result.result.all())} changes created and applied"
            return f"FAIL: {fails_lst} changes could not be applied"
        return f"OK, {len(result.result.all())} changes created"
