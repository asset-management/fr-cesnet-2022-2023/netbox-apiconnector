# Generated by Django 4.1.7 on 2023-04-12 07:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('netbox_apiconnector', '0025_alter_apiconnectorresult_last_applied_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='apiconnectorresultchange',
            name='obj',
            field=models.BigIntegerField(default=1),
            preserve_default=False,
        ),
    ]
