def try_get(model, pk):
    try:
        return model.objects.get(pk=pk)
    except model.DoesNotExist:
        return None
