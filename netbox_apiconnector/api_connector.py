import datetime
from threading import Thread
from django.utils import timezone
from django.core.exceptions import FieldDoesNotExist
from jinja2 import Template, UndefinedError
from django.apps import apps
import requests
import json

from requests import JSONDecodeError

from netbox_apiconnector.models import ApiConnectorResult, ApiConnectorResultChange
from django.contrib.contenttypes.models import ContentType
from .status_codes import *


# JINJA FUNCTIONS
def parse_inet_date(d):
    """
    Custom function passed to the Jinja2 environment created for the purposes of MUNI.
    @param d: date string in the format dd.mm.yyyy hh:mm:ss
    @return: Assignable Django date.
    """
    if not d:
        return None
    date_lst = d.split('.')
    if len(date_lst) < 3:
        return None
    date = datetime.datetime(int(date_lst[2][:4]), int(date_lst[1]), int(date_lst[0]))
    return date.date()


def get_obj(app, model, *args, **kwargs):
    """
    Custom function passed to the Jinja2 environment.
    Finds an object contained in the specified model by the search parameters passed via the '**kwargs' dictionary.
    @param app: e.g. dcim
    @param model: e.g. site
    @param args: not used
    @param kwargs: dictionary of filters
    @return a string in a format later parsed by the functions applying changes.
    """
    m = apps.get_model(app, model)
    q = m.objects.filter(**kwargs)
    if q:
        return f"{app}|{model}|{q.first().pk}"
    else:
        return f"{app}|{model}|{None}"


# JINJA FUNCTIONS
def render_template(template, data):
    """
    @param template: Jinja2 template string
    @param data: dictionary
    @return: rendered template
    """
    return Template(template).render(data)


def pack_model(obj):
    """
    @param obj: netbox object
    @return: encoded object identifiable by its app, model and primary key, e.g. dcim|site|pk
    """
    content_type = ContentType.objects.get_for_model(obj)
    return f"{content_type.app_label}|{content_type.model}|{obj.pk}"


def check_templates(templates):
    """
    @param templates: Jinja2 template strings
    @return: checks the validity of templates and loads the mapping rules to a dictionary.
    @raise: ValueError, if template could not be loaded.
    """
    mappings = []
    template = ""
    try:
        for template in templates:
            mappings.append(json.loads(template))
        return mappings
    except Exception as e:
        raise ValueError(f"{e} in \n {template}")


def get_netbox_object(m, entry):
    """
    Looks up a netbox object by fields marked with '@' in the mapping rules.
    Fields get the value from the API.
    @param m: netbox model
    @param entry: response from an API
    @return: netbox object
    """
    lookups = {}
    for k, v in entry.items():
        if len(k) > 0 and k[0] == "@":
            lookups[k[1:]] = v
    return m.objects.filter(**lookups)


def check_key(netbox_object, k, result):
    """
    @param netbox_object: netbox object
    @param k: key of a dictionary with mapping rules. Attribute of a netbox object.
    @param result: result created by running the api connector.
    @return: True if the netbox object posses the attribute 'k' from the mapping rules
    """
    if len(k) > 0 and k[0] == "@":
        return False
    if len(k) > 0 and k[0] == "$":
        k = k[1:]
    if hasattr(netbox_object, k):
        return True
    result.delete()
    raise FieldDoesNotExist(f"Object \'{netbox_object}\' has no field \'{k}\'")


def handle_nested_dict(k, v, obj):
    """
    includes only changed attributes of custom field data fields in the resulting changes.
    @param k: attribute
    @param v: value
    @param obj: netbox object
    @return: current and new values of nested dictionaries (basically only custom_field_data)
    """
    new_dict = {}
    current_dict = {}
    tmp = getattr(obj, k)
    for current_k in tmp.keys():
        if tmp[current_k] is not None:
            new_dict[current_k] = tmp[current_k]
            current_dict[current_k] = tmp[current_k]
    for new_k in v.keys():
        new_dict[new_k] = v[new_k]
    return current_dict, new_dict


def put_current_val(netbox_object, entry, k, v, current):
    """
    @param netbox_object: netbox object
    @param entry: response from an API
    @param k: attribute
    @param v: value of the attribute
    @param current: current value
    @return: true if the current value of an attribute was successfully inserted
    """
    if len(k) > 0 and k[0] == "@":
        return False
    if len(k) > 0 and k[0] == "$":
        current[k] = pack_model(getattr(netbox_object, k[1:]))
    elif isinstance(v, dict):
        current[k], entry[k] = handle_nested_dict(k, v, netbox_object)
    else:
        current[k] = getattr(netbox_object, k)
    return True


def make_request(url, connector):
    """
    Sends a request to an API. Supports only JSON.
    @param url: url of the API
    @param connector: API connector to be used
    @return: data obtained (a python dict)
    """
    headers = {'Content-type': connector.content_type}
    headers.update(json.loads(connector.additional_headers))
    r = requests.request(connector.method,
                         url,
                         headers=headers,
                         data=json.loads(connector.additional_body),
                         timeout=30)
    print(url)
    try:
        if r.status_code == 200:
            rjson = r.json()
            if isinstance(rjson, dict):
                if 'header' in rjson:
                    header = rjson['header']
                body = rjson['data']
            else:
                body = rjson
            return body
        else:
            raise ConnectionError("Connection was not successful")
    except JSONDecodeError as e:
        connector.message += f"URL: \'{url}\' JSON decode error, possibly invalid JSON response from API\n"
        connector.save()
    return {}


def create_result(connector, templates, m):
    """
    Creates result of running an API connector
    @param connector: the API connector
    @param templates: Jinja2 templates with mapping rules generated by making the requests
    @param m: model
    @return: run result
    """
    pks = []
    same = 0
    mappings = check_templates(templates)
    result = ApiConnectorResult.objects.create(connector=connector)
    for mapping in mappings:
        for entry in mapping:
            try:
                netbox_object = get_netbox_object(m, entry)
            except KeyError:
                result.delete()
                raise AttributeError(f"Object could not be found by lookup fields marked with \'@\'")

            if netbox_object:
                netbox_object = netbox_object.first()
                current = {}
                new_val = {}
                for k, v in entry.items():
                    check_key(netbox_object, k, result)
                    if put_current_val(netbox_object, entry, k, v, current):
                        new_val[k] = entry[k]
                if current != new_val:
                    obj = ApiConnectorResultChange.objects.create(run_result=result,
                                                                  obj=netbox_object.pk,
                                                                  current_value=current,
                                                                  new_value=new_val)
                    pks.append(obj.pk)
                else:
                    same += 1

            elif not netbox_object and connector.create_new_objects:
                obj = ApiConnectorResultChange.objects.create(run_result=result,
                                                              obj=0,
                                                              new_value=entry)
                pks.append(obj.pk)
            else:
                continue

    result.save()
    if len(pks) == 0:
        result.delete()
    set_status(connector, SUCCESS, f"{len(pks)} new changes found\n"
                                   f"{same} objects without any changes found\n")
    return result


def static_converter(connector):
    """
    Runs a standard API connector and renders a template.
    @param connector: the API connector
    @return: run result
    """
    set_status(connector, RUNNING, "")
    if connector.url is None:
        raise AttributeError("Empty url")
    templates = []
    body = make_request(connector.url, connector)
    m = apps.get_model(connector.obj.app_label, connector.obj.model)
    templates.append(render_template(connector.mapping,
                                     {"response": body, 'get_obj': get_obj, 'parse_date': parse_inet_date}))
    return create_result(connector, templates, m)


def dynamic_converter(connector):
    """
    Runs a standard API connector and renders a template and adds it to a list of templates.
    @param connector: the API connector
    @return: run result
    """
    set_status(connector, RUNNING, "")
    if connector.pattern_url is None:
        raise AttributeError("Empty pattern url")
    m = apps.get_model(connector.obj.app_label, connector.obj.model)
    try:
        empty_url = render_template(connector.pattern_url, {"object": m})
    except UndefinedError as e:
        empty_url = None
    templates = []
    i = 1
    non_existent = 0
    for obj in m.objects.all():
        if i % 1000 == 0:
            connector.message += f"processed {i} objects\n"
            connector.save()
        url_template = render_template(connector.pattern_url, {"object": obj})
        if url_template == empty_url:
            i += 1
            continue
        body = make_request(url_template, connector)
        if len(body) == 0:
            non_existent += 1
        else:
            templates.append(render_template(connector.mapping,
                                             {"response": body, 'get_obj': get_obj, 'parse_date': parse_inet_date}))
        i += 1
    connector.message += f"{non_existent} objects could not be retrieved from the API\n"
    connector.save()
    return create_result(connector, templates, m)


def mapper(m, change, connector):
    """
    Executes a single change
    @param m: model
    @param change: the change
    @param connector: the API connector
    @return: the number of failed changes, 0 or 1.
    """
    change.last_applied = timezone.now()
    new_dict = change.new_value
    new_obj_dict = {}
    current_obj_dict = {}
    for k, v in new_dict.items():
        if len(k) > 0 and k[0] == "@":
            # k = k[1:]
            continue
        current_obj_dict[k] = v
        if len(k) > 0 and k[0] == "$":
            k = k[1:]
            model_info = v.split("|")
            fk_model = apps.get_model(model_info[0], model_info[1])
            v = fk_model.objects.get(pk=model_info[2])
        new_obj_dict[k] = v
    netbox_object = m.objects.filter(pk=change.obj)
    if (connector.create_new_objects and netbox_object) or not connector.create_new_objects:
        if netbox_object.update(**new_obj_dict) == 0:
            set_status(change, FAIL, f"Object {change.obj} could not be found")
            return 1
    else:
        new_netbox_object, created = m.objects.update_or_create(**new_obj_dict)
        change.obj = new_netbox_object.pk
    change.current_value = current_obj_dict
    set_status(change, SUCCESS, "")
    return 0


def apply_changes_by_pk(pks):
    """
    applies changes in bulk by their primary keys
    @param pks: a list of primary keys
    @return: the number of failed changes
    """
    failed = 0
    result = None
    for pk in pks:
        change = ApiConnectorResultChange.objects.get(pk=int(pk))
        result = change.run_result
        result.last_applied = timezone.now()
        connector = result.connector
        m = apps.get_model(connector.obj.app_label, connector.obj.model)
        try:
            failed += mapper(m, change, connector)
        except Exception as e:
            failed += 1
            set_status(change, FAIL, f"Failed. Exception: {e}")
        result.save()
    if result and len(pks) - failed == len(result.result.all()):
        set_status(result, SUCCESS, "")
    return failed


def apply_changes(changes, result):
    """
    Applies changes
    @param changes: the change (an object, not a pk)
    @param result: run result that the change is a part of
    @return: the number of failed changes, 0 or 1
    """
    failed = 0
    connector = result.connector
    m = apps.get_model(connector.obj.app_label, connector.obj.model)
    for change in changes:
        try:
            failed += mapper(m, change, connector)
        except Exception as e:
            failed += 1
            set_status(change, FAIL, f"Failed. Exception: {e}")
    return failed


def apply_results_by_pk(pks):
    """
    Applies results by their primary keys.
    @param pks: a list of primary keys
    @return: a list of the number of failed changes
    """
    fails_lst = []
    for pk in pks:
        result = ApiConnectorResult.objects.get(pk=pk)
        result.last_applied = timezone.now()
        changes = result.result.all()
        set_status(result, RUNNING)
        fails = apply_changes(changes, result)
        if fails != 0:
            set_status(result, FAIL, f"{fails} changes failed to be applied.")
        else:
            set_status(result, SUCCESS, "")
        fails_lst.append(fails)
        result.save()
    return fails_lst


def start_thread(func, params):
    """
    Starts a new thread
    @param func: a function
    @param params: arguments passed to the function
    @return: None
    """
    t = Thread(target=func, args=(params,))
    t.daemon = False
    t.start()


def static_converter_worker(connector):
    """
    Worker used in threads, handles exceptions and saves them to the 'Messages' field
    @param connector: an API connector
    @return: None
    """
    try:
        static_converter(connector)
    except Exception as e:
        set_status(connector, FAIL, e)


def dynamic_converter_worker(connector):
    """
    Worker used in threads, handles exceptions and saves them to the 'Messages' field
    @param connector: an API connector
    @return: None
    """
    try:
        dynamic_converter(connector)
    except Exception as e:
        set_status(connector, FAIL, e)
