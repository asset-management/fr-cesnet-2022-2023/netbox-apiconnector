from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from netbox.models import NetBoxModel


class ApiConnector(NetBoxModel):
    name = models.CharField(max_length=255, unique=True)
    POST = 'POST'
    GET = 'GET'
    PUT = 'PUT'
    DELETE = 'DELETE'
    PATCH = 'PATCH'
    METHOD_CHOICES = [
        (POST, 'POST'),
        (GET, 'GET'),
        (PUT, 'PUT'),
        (DELETE, 'DELETE'),
        (PATCH, 'PATCH'),
    ]
    method = models.CharField(max_length=7, choices=METHOD_CHOICES, default=GET)
    content_type = models.CharField(max_length=255, default="application/json")
    run_in_background = models.BooleanField(default=False)
    additional_headers = models.TextField(blank=True, null=True, default="{}", help_text="Additional header")
    additional_body = models.TextField(blank=True, null=True, default="{}", help_text="Additional body")
    url = models.CharField(max_length=1023, blank=True, null=True,
                           help_text="Only applicable to the \'Run mapping\' button")
    pattern_url = models.CharField(max_length=1023, blank=True, null=True,
                                   help_text="Only applicable to the \'Run pattern mapping\' button")
    obj = models.ForeignKey(to=ContentType, on_delete=models.PROTECT,
                            help_text='The type of NetBox object that is going to be changed')
    create_new_objects = models.BooleanField(default=False)
    status = models.CharField(max_length=50, default="CREATED")
    mapping = models.TextField(default="""[
    {%- for entry in response %}
        {
            ---REPLACE WITH YOUR CUSTOM RULES---
        }
        {%- if not loop.last -%}
            ,
        {%- endif %}
    {%- endfor %}
]""", help_text="Jinja2 mapping of NetBox fields to API data stored in the \'entry\' dictionary")
    message = models.TextField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse("plugins:netbox_apiconnector:apiconnector", args=[self.pk])

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def last_run_result(self):
        try:
            return self.connector.order_by('-date')[0]
        except IndexError:
            return None


class ApiConnectorResult(NetBoxModel):
    connector = models.ForeignKey(to=ApiConnector, on_delete=models.CASCADE, related_name="connector")
    date = models.DateTimeField(auto_now_add=True)
    last_applied = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=50, default="CREATED")
    message = models.TextField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse("plugins:netbox_apiconnector:apiconnectorresult", args=[self.pk])

    def __str__(self):
        return self.connector.name + " run at " + str(self.date.strftime("%d %b %Y, %H:%M:%S"))

    def __unicode__(self):
        return self.connector.name + " run at " + str(self.date.strftime("%d %b %Y, %H:%M:%S"))


class ApiConnectorResultChange(NetBoxModel):
    run_result = models.ForeignKey(to=ApiConnectorResult, on_delete=models.CASCADE, related_name="result")
    obj = models.BigIntegerField(blank=True, null=True)
    current_value = models.JSONField(blank=True, null=True)
    new_value = models.JSONField()
    last_applied = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=50, default="CREATED")
    message = models.TextField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse("plugins:netbox_apiconnector:apiconnectorresultchange", args=[self.pk])

    def __str__(self):
        return f"Change {self.pk} created by {self.run_result.connector.name}"

    def __unicode__(self):
        return f"Change {self.pk} created by {self.run_result.connector.name}"
