from extras.plugins import PluginMenuItem, PluginMenu

menu = PluginMenu(
    label='API Connector',
    groups=(
        (
            '',
            (
                PluginMenuItem(
                    link="plugins:netbox_apiconnector:apiconnector_list",
                    link_text="Configurations",
                    permissions=['netbox_apiconnector.view_apiconnector'],
                ),
                PluginMenuItem(
                    link="plugins:netbox_apiconnector:apiconnectorresult_list",
                    link_text="Results",
                    permissions=['netbox_apiconnector.view_apiconnectorresult'],
                ),
                PluginMenuItem(
                    link="plugins:netbox_apiconnector:apiconnectorresultchange_list",
                    link_text="Changes",
                    permissions=['netbox_apiconnector.view_apiconnectorresultchange'],
                ),
            )
        ),
    ),
    icon_class='mdi mdi-sync'
)
