TABLE_STATUS = """{% if record.status == "SUCCESS" %}
                    <span class="badge bg-success">{{ record.status }}</span>
                {% elif record.status == "CREATED" %}
                    <span class="badge bg-warning">{{ record.status }}</span>
                {% elif record.status == "FAIL" %}
                    <span class="badge bg-danger">{{ record.status }}</span>
                {% elif record.status == "RUNNING" %}
                    <span class="badge bg-info">{{ record.status }}</span>
                {% else %}
                    {{ record.status }}        
            {% endif %}"""

CHANGE_TABLE_STATUS = """{% if record.status == "SUCCESS" %}
                    <span class="badge bg-success">{{ record.status }}</span>
                {% elif record.status == "FAIL" %}
                    <span class="badge bg-danger">{{ record.status }}</span>
                {% elif record.status == "CREATED" and record.current_value is None %}
                    <span class="badge bg-warning">{{ record.status }}</span>
                {% elif record.new_value == record.current_value and record.last_applied is None %}
                    <span class="badge bg-info">{{ record.status }}</span>
                {% elif record.new_value != record.current_value and record.last_applied is None %}
                    <span class="badge bg-warning">{{ record.status }}</span>
                {% else %}
                    {{ record.status }}        
            {% endif %}"""

CHANGE_OBJ = """{% load api_connector_tags %}
        {% get_name record as name %}
        {% if record.obj == 0 %}
            <span class="badge bg-warning">New object</span>
        {% elif name is None %}
            <span class="badge bg-danger">Object does not exist</span>
        {% else %}
            <a href="{% get_url record %}"><span class="badge bg-primary">{{ name }}</span></a>
        {% endif %}"""

CHANGE_NEW_VAL = """{% load api_connector_tags %}
            {% get_name record as name %}
            {% if record.obj == 0 and record.last_applied is None %}
                <div class="badge bg-warning">{{ record.new_value }}</div>
            {% elif name is None %}
                <div class="badge bg-secondary">{{ record.new_value }}</div>
            {% elif record.new_value == record.current_value and record.last_applied is not None %}
                <div class="badge bg-success">{{ record.new_value }}</div>
            {% elif record.new_value == record.current_value and record.last_applied is None %}
                <div class="badge bg-info">{{ record.new_value }}</div>
            {% elif record.new_value != record.current_value and record.last_applied is None %}
                <div class="badge bg-warning">{{ record.new_value }}</div>
            {% else %}
                <div class="badge bg-danger">{{ record.new_value }}</div>
            {% endif %}"""

RESULT_APPLY_BUTTON = """<a href="{% url 'plugins:netbox_apiconnector:apiconnector_apply_all_conversion_by_pk' pk=record.pk %}" type="button" class="btn btn-sm btn-indigo">
    <i class="mdi mdi-sync"></i>Apply</a>"""

RESULT_CHANGE_APPLY_BUTTON = """<a href="{% url 'plugins:netbox_apiconnector:apiconnector_apply_conversion_by_pk' pk=record.pk %}" type="button" class="btn btn-sm btn-indigo">
    <i class="mdi mdi-sync"></i>Apply</a>"""

RESULT_CHANGES_COUNT = """{% with changes_count=record.result.count %}
                                        {% if changes_count %}
                                            <a href="{% url 'plugins:netbox_apiconnector:apiconnectorresultchange_list' %}?run_result={{ record.pk }}">{{ changes_count }}</a>
                                        {% else %}
                                            {{ ''|placeholder }}
                                        {% endif %}
                                    {% endwith %}"""
