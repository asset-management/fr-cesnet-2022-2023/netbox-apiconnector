from rest_framework import serializers
from netbox.api.serializers import NetBoxModelSerializer
from netbox_apiconnector.models import *


class ApiConnectorSerializer(NetBoxModelSerializer):
    class Meta:
        model = ApiConnector
        fields = '__all__'


class ApiConnectorResultSerializer(NetBoxModelSerializer):
    class Meta:
        model = ApiConnectorResult
        fields = '__all__'


class ApiConnectorResultChangeSerializer(NetBoxModelSerializer):
    class Meta:
        model = ApiConnectorResultChange
        fields = '__all__'
