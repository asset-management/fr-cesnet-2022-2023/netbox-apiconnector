# api/views.py
from netbox.api.viewsets import NetBoxModelViewSet
from netbox_apiconnector.models import *
from .serializers import *
from .. import filtersets


class ApiConnectorViewSet(NetBoxModelViewSet):
    queryset = ApiConnector.objects.all()
    serializer_class = ApiConnectorSerializer


class ApiConnectorResultViewSet(NetBoxModelViewSet):
    queryset = ApiConnectorResult.objects.all()
    serializer_class = ApiConnectorResultSerializer
    filterset_class = filtersets.ApiConnectorResultFilterSet


class ApiConnectorResultChangeViewSet(NetBoxModelViewSet):
    queryset = ApiConnectorResultChange.objects.all()
    serializer_class = ApiConnectorResultChangeSerializer
    filterset_class = filtersets.ApiConnectorResultChangeFilterSet
